NOTES.md
--------



Profiles:

Linux
Windows

Roles (tmgcore::role::generic)

Generic
    - The Generic roles includes the WIndows profile if Windows
PuppetMaster



TwinCAT boot project target directory:
C:\TwinCAT\3.1\Boot\Plc\

Port_xxx.app - Binary file of the PLC project
Port_xxx.autostart - Empty file that activates the Autostart option
Port_xxx.cid - File containing the Compileinfo_IDs
Port_xxx.crc - File containing the checksum of the PLC project
Port_xxx.occ - Symbolics of the PLC program
Port_xxx.oce - File contains the changes of the event classes at the time of an OnlineChange, which are used in a PLC
Port_xxx.ocm - Description file of the mapping configuration
Port_xxx_act.tizip - Achrive folder containig the COMPILEINFO file of the currently running PLC project
Port_xxx.bootdata - Boot file that saves the persistent data
Port_xxx.bootdata-old - Backup file for the persistent data
PLC_Name.tpzip - Archive folder of the PLC project
PLC_Name.tmc - TC3 module description file
PLC_name.tpy - TC2 PLC description file


Other File(s)/Locations information: 
File Description Further Information
Engineering / XAE
*.sln Visual Studio Solution file, hosts TwinCAT and
non-TwinCAT projects
*.tsproj TwinCAT project, collection of all nested
TwinCAT projects, such as TwinCAT C++ or
TwinCAT PLC project
_Config/ Folder contains further configuration files (*.xti)
that belong to the TwinCAT project.
See menu Tools| Options|
TwinCAT| XAEEnvironment| File
Settings
_Deployment/ Folder for compiled TwinCAT C++ drivers
*.tmc TwinCAT Module Class file (XML-based) See TwinCAT Module
Class Editor (TMC)
*.rc Resource file See Set version/vendor
information
*.vcxproj.* Visual Studio C++ project files
*ClassFactory.cpp/.h Class Factory for this TwinCAT driver
*Ctrl.cpp/.h Upload and remove drivers for TwinCAT UM
platform
*Driver.cpp/.h Upload and remove drivers for TwinCAT RT
platform
*Interfaces.cpp/.h Declaration of the TwinCAT COM interface
classes
*W32.cpp./.def/.idl
*.cpp/.h One C++/Header file per TwinCAT module in
the driver. Insert user code here.
Resource.h Required by *.rc file
TcPch.cpp/.h Used for creating precompiled headers
%TC_INSTALLPATH%
\Repository\<Vendor>\<PrjNam
e>\<Version>\<Platform>\*.tmx
Compiled driver that is loaded via the
TcLoader.
C:\TwinCAT\3.x\Repository\C++ Module
Vendor\Untitled1\0.0.0.1\TwinCAT RT
*\Unititled1.tmx
See Versioned C++
Projects
%TC_INSTALLPATH%
\CustomConfig\Modules\*
Published TwinCAT driver package
usually C:
\TwinCAT\3.x\CustomConfig\Modules\*
See Export modules
Runtime / XAR
%TC_BOOTPRJPATH%
\CurrentConfig\*
Current configuration setup
Windows: C:\TwinCAT\3.x\Boot
TwinCAT/BSD: /usr/local/etc/TwinCAT/3.x/Boot
%TC_DRIVERAUTOINSTALLP
ATH% \*.sys/pdb
Compiled, platform-specific driver that is loaded
via the operating system.
Windows: C:\TwinCAT\3.x\Driver\AutoInstall
(system loaded)
TwinCAT/BSD: <not available>
%TC_INSTALLPATH%
\Boot\Repository\<Vendor>\<Prj
Name>\<Version>\*.tmx
Compiled platform-specific driver that is loaded
via the TcLoader.
Windows: C:\TwinCAT\3.x\Boot\Repository\C++
Module Vendor\Untitled1\0.0.0.1\Untitled1.tmx
TwinCAT/BSD: /usr/local/etc/TwinCAT/3.x/
Boot\/Repository\C++ Module
Vendor\Untitled1/0.0.0.1/Untitled1.tmx
TwinCAT C++ project files
TwinCAT 3 Version: 1.2 15
File Description Further Information
%TC_BOOTPRJPATH%
\TM\OBJECTID.tmi
TwinCAT Module Instance file
Describes variables of the driver
File name is ObjectID.tmi
Windows: C:\TwinCAT\3.x\Boot\TMI\OTCID.tmi
TwinCAT/BSD:
/usr/local/etc/TwinCAT/3.x/Boot/TMI/OTCID.tmi
Temporary files
*.sdf IntelliSense Database
*.suo / *.v12.suo User-specific and Visual Studio-specific files
*.tsproj.bak Automatically generated backup file from tsproj
ipch/ Intermediate directory created for precompiled
headers
